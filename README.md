Custom cache module add possibility to replace permanent caches to time limited caches. It's usefull for sites with a lot of nodes, drupal generates a lot of page caches without time limit which significantly increase DB size with time.

<h3>Installation</h3>
Install it via composer like any other drupal module and add a configuration to a settings.php

<h3>Configuration</h3>
You need to add a configuration to your settings.php:

Setup a cache time limit, as example for one day:
<code>
$settings['custom_cache_melt_time'] = 86400;
</code>

Add a  cache backend service:
<code>
$settings['cache']['bins']['render'] = 'cache.backend.custom_cache';
$settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.custom_cache';
$settings['cache']['bins']['page'] = 'cache.backend.custom_cache';
</code>

Exclude cids which we want to save:
<code>
$settings['custom_cache_exclude_cids'] = [
  '/node/',
  '/taxonomy/term/',
  '/sites/default/files/',
  '/user/'
];
</code>