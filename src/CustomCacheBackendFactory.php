<?php

namespace Drupal\custom_cache;

use Drupal\Core\Cache\DatabaseBackendFactory;
use Drupal\custom_cache\CustomCacheDatabaseBackend;

/**
 * Defines a database cache backend factory with a maximum age.
 */
class CustomCacheBackendFactory extends DatabaseBackendFactory {

  /**
   * {@inheritdoc}
   */
  function get($bin) {
    return new CustomCacheDatabaseBackend($this->connection, $this->checksumProvider, $bin);
  }

}
