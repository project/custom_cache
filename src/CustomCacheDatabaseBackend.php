<?php

namespace Drupal\custom_cache;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\DatabaseBackend;
use Drupal\Core\Site\Settings;

/**
 * Defines a database cache backend with a maximum lifetime.
 */
class CustomCacheDatabaseBackend extends DatabaseBackend {

  /**
   * {@inheritdoc}
   */
  public function setMultiple(array $items) {
    $excludeCids = Settings::get('custom_cache_exclude_cids', []);
    $NewItems = [];
    foreach ($items as $cid => &$item) {
      if(!empty($excludeCids)){
        foreach ($excludeCids as $excludeCid){
          if(strpos($cid, $excludeCid) !== FALSE){
            unset($items[$cid]);
            continue;
          }
        }
      }
      if (!isset($item['expire']) || $item['expire'] === Cache::PERMANENT) {
        // Enforce a maximum lifetime.
        $item['expire'] = \Drupal::time()->getRequestTime() + Settings::get('custom_cache_melt_time', 86400);
      }
      $cid = self::clearCid($cid);
      $NewItems[$cid] = $item;

    }
    if(!empty($NewItems)){
      parent::setMultiple($NewItems);
    }
  }
  public function getMultiple(&$cids, $allow_invalid = FALSE){
    if(!empty($cids)){
      foreach ($cids as $key => $cid){
        $cids[$key] = self::clearCid($cid);
      }
    }
    return parent::getMultiple($cids, $allow_invalid = FALSE);
  }
  private static function clearCid($cid){
    if(strpos($cid, '[session]=') !== FALSE || strpos($cid, '[user.permissions]=') !== FALSE){
      $cid_keys = explode(':', $cid);
      foreach ($cid_keys as $key => &$cid_key){
        if(strpos($cid_key, '[session]=') !== FALSE){
          unset($cid_keys[$key]);
        }
        if(strpos($cid_key, '[user.permissions]=') !== FALSE){
          $roles = \Drupal::currentUser()->getAccount()->getRoles();
          $cid_keys[$key] = '[user.permissions]='.implode('-', $roles);
        }
      }
      $cid = implode(':', $cid_keys);
    }
    if(strpos($_SERVER['HTTP_USER_AGENT'],'Chrome-Lighthouse') !== FALSE) {
      $cid = 'gps:'.$cid;
    }
    // Alter
    \Drupal::moduleHandler()->alter('custom_cache_cid', $cid);
    // ---
    return $cid;
  }
}
